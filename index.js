var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var client = require('./client.js');
var dao = require('./dao.js');

app.use('/static', express.static('static')); 

app.get('/', function(req, res){
 	res.sendFile(__dirname + '/index.html');
});

app.post('/admin/redeploy', function(req, res){
	console.log('redeploy at : ' + new Date())
 	var exec = require('child_process').exec;
    exec("sh deploy.sh", function(err, out, code) {
        process.stdout.write(out);
    });
});

io.on('connection', function(socket) {
	var token = false;
	socket.on('location', function(gps) {
		if((typeof gps) == 'string') {
			gps = JSON.parse(gps);
		}
		var r = {'token': token, 'lat': gps.lat, 'lon': gps.lon, v: 1};
		console.log(typeof gps, r);
		io.emit('location_log', r);
		var tiles = client.translateCoordinates(gps.lat, gps.lon);
  		console.log(gps, tiles);
		socket.emit('position', tiles);
 	});

 	socket.on('disconnect', function() {
        console.log('user disconnected: ');
    });

 	socket.on('login', function(json) {
 		var info = client.register(json.token, socket);
 		token = info.token;
 		console.log(info);
 		socket.emit('login', info);
 		io.emit('login_log', info);
 		var t = { x: 78330.77532444445, y: 45650.4180547578 };
 		
 		socket.emit('position',  t);
 	});

 	socket.on('invoke_position', function(data) {
        console.log('invoke_position: ', data);
        var gps = data.gps;
        var tiles = client.translateCoordinates(gps.lat, gps.lon);
        var c = client.getUser(data.token);
        console.log('invoke_position: ', data, tiles);
        c.socket.emit('position', tiles);
    });
});

http.listen(3000, function(){
	console.log('Welcome. listening on *:3000');
});
