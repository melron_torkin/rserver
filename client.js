var TO_RADIANS = 0.0174532925;
var S_ZOOM = 131072;

var uuid = require('node-uuid');

var connected_users = {};

module.exports.translateCoordinates = function (lat, lon) {
		// var lon_rad = lon * TO_RADIANS;
		var lat_rad = lat * TO_RADIANS;

	    var tileX = ((lon + 180) / 360) * S_ZOOM;
		var tileY = (1 - (Math.log(Math.tan(lat_rad) + 1.0/Math.cos(lat_rad)) / Math.PI)) * S_ZOOM / 2.0;
		return {x: tileX, y: tileY};
	};

module.exports.register = function (token, socket) {
		if(!token) {
			token = uuid.v4();
		}

		connected_users[token] = {
			token: token,
			socket: socket,
			account: {},
		}
		return connected_users[token];
	};
module.exports.getUser = function (token) {
		return connected_users[token];
	};

module.exports.disconnect = function (token) {
		connected_users[token] = null;
	};
